Nama Saya Dimas Wahyu Pratama
Disini saya membuat aplikasi android menggunakan react native expo
Pada app ini kita dapat melihat list movie
Disini saya menggunakan public api dari OMDb API berikut adalah link dari public API nya http://www.omdbapi.com/
dan untuk pengembangan aplikasi ini saya menggunakan android ASUS Zenfone Max Pro M2 dengan versi Andoid 9 

Berikut ini adalah link dari repository gitlab untuk final project yang saya kerjakan :
https://gitlab.com/Dimas1711/finalprojectmymovieapp

Berikut ini adalah link dari mockup figma dari final project yang saya buat:
https://www.figma.com/file/S8UVJgQwsRsg6SsmeE85UP/Final-Project?node-id=0%3A1

Berikut ini adalah link download build apk expo dari aplikasi yang saya kerjakan:
https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40dimas_wahyu17/MyMovieApp-31ec2df2f86a4f959f321474198d3d63-signed.apk