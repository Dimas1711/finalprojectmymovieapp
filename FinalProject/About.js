import { AntDesign } from '@expo/vector-icons';
import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';

const About = () => {
    return (
        <ScrollView>
            <View style={styles.container}>
            <View style={styles.text}>
                <Text style={{fontSize:36, color:"#003366", fontWeight:"bold", fontFamily:"Roboto"}}>About Me</Text>
            </View>
            <Image source={require('./images/IMG.jpg')} style={styles.profileImg}/>
            <View style={styles.text}>
                <Text style={{fontSize:24, color:"#003366", fontFamily:"Roboto"}}>Dimas Wahyu Pratama</Text>
            </View>
            <View style={styles.text}>
                <Text style={{fontSize:16, color:"#003366", fontFamily:"Roboto"}}>Follow Me at My Social Media Account</Text>
            </View>
            <View style={styles.fb}>
                <AntDesign name="facebook-square" size={40} color="#0320ff" />
                <Text style={{marginStart:10}}></Text>
                <Text style={{fontWeight:"bold"}}>dimas.wahyupratama.50</Text>
            </View>
            <View style={styles.inst}>
                <AntDesign name="instagram" size={40} color="#eb54bb" style={{alignSelf:"center"}}/>
                <Text style={{marginStart:20 , fontWeight:"bold"}}>@dimas_wp17</Text>
            </View>
            <View style={styles.twit}>
                <AntDesign name="twitter" size={40} color="#5bc4f5" style={{alignSelf:"center"}}/>
                <Text style={{marginStart:20, fontWeight:"bold"}}>@dimas_wp17</Text>
            </View>
        </View>
      </ScrollView>
    )
}

export default About

const styles = StyleSheet.create({
    container: {
        flex:1,
        paddingTop:20,
        paddingBottom:30
      },
      text:{
        alignItems:"center",
        justifyContent:"center",
        marginTop:10
      },
      profileImg:{
        width: 100,
        height:100,
        borderRadius: 40,
        borderWidth:2,
        borderColor:"#000000",
        marginTop:20,
        borderWidth:1,
        alignSelf:"center",
      },
      fb:{
          flexDirection:"row",
          alignItems:"center",
          alignSelf:"center",
          marginTop:30,
      },
      inst:{
          flexDirection:"row",
          alignSelf:"center",
          alignItems:"center",
          marginTop:20,
      },
      twit:{
          flexDirection:"row",
          alignSelf:"center",
          alignItems:"center",
          marginTop:20,
      },
})
