import React from 'react'
import { StyleSheet, Text, View, Button, TextInput, Alert, TouchableOpacity, Image} from 'react-native'

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          userName: '',
          password: '',
          isError: false,
        }
    }
    loginHandler(){
        if(this.state.password == "" || this.state.userName == "")
        {
            Alert.alert(
                "Warning",
                "Field Tidak Boleh Kosong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
        }
        else
        {
            this.props.navigation.navigate('Home', {userName : this.state.userName})
        }
    }
    render()
    {
        return (
            <View style={styles.container}>
                <Text style={{fontSize:50, textAlign:"center"}}>Login</Text>
                <Image source={require('./images/logo.png')} style={{marginTop:10, alignSelf:"center"}}/>
                <View style={{marginTop:30}}>
                    <Text style={{marginStart:10, marginTop:20, marginBottom:8}}>Username</Text>
                    <TextInput style={styles.input} placeholder="Username"
                    onChangeText={userName => this.setState({ userName })}></TextInput>
                    <Text style={{marginStart:10, marginBottom:8}}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true} placeholder="Password"
                    onChangeText={password => this.setState({ password })}></TextInput>
                    <TouchableOpacity style={styles.button} onPress={() => this.loginHandler()}>
                        <Text style={{fontSize:20,color:"#fff"}}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop:50,
        backgroundColor:"#E3E3E3"
    },
    input:{
        borderWidth:1,
        marginStart:10,
        marginEnd:10,
        marginBottom:10,
    },
    button:{
        backgroundColor:"#43C6D8",
        width:200,
        height:50,
        marginTop:10,
        borderRadius:8,
        alignItems:"center",
        justifyContent:"center",
        alignSelf:"center"
    },
})
