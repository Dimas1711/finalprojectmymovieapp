import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View , Image, TouchableOpacity, Button, Alert} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 


export default class ListItem extends Component {
    render()
    {
        let list = this.props.list;
        return(
         <View style={styles.container}>
             <TouchableOpacity style={styles.itemlist}>
             <Image source={{ uri: `${list.Poster}` }} style={styles.Image} />
                <View style={styles.Text}>
                    <Text style={{fontWeight:"bold"}}>{list.Title}</Text>
                    <Text>{list.Year}</Text>
                    <Text>{list.Type}</Text>
                    <Text>{list.imdbID}</Text>
                </View>
             </TouchableOpacity>
         </View>   
        )
    }
}



const styles = StyleSheet.create({
    container:{
        flex:1,
        margin:8,
    },
    itemlist:{
        marginBottom:8,
        flexDirection:"row",
        borderRadius:8,
        borderWidth:1
    },
    Image:{
        width: 100,
        height: 120,
        margin:8,
        resizeMode:"center",
    },
    Text:{
        marginTop:20,
    }
    
});