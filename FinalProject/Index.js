import 'react-native-gesture-handler'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'

import HomeScreen from './Home'
import LoginScreen from './Login'
import AboutScreen from './About'
import DetailScreen from './Detail'

const Stack = createStackNavigator() 

const Index = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName = "Login">
                <Stack.Screen name="Login" component={LoginScreen} options={{headerShown:false}}/>
                <Stack.Screen name="Home" component={HomeScreen} options={{headerLeft:null}}/>
                <Stack.Screen name="About" component={AboutScreen}/>
                <Stack.Screen name="Detail" component={DetailScreen}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Index

const styles = StyleSheet.create({})
