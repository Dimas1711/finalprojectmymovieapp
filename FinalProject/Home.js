import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator, Alert, Button, TouchableOpacity} from 'react-native';
import Axios from 'axios';
import { FontAwesome5 } from '@expo/vector-icons'; 
import { MaterialCommunityIcons } from '@expo/vector-icons'; 

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false,
    };
  }
  
  
  // Mount User Method
  componentDidMount() {
    this.getMovieData()
  }

  //   Get Api Users
  getMovieData = async () => {
    try {
      const response = await Axios.get(`http://www.omdbapi.com/?apikey=133decb5&s=dragon+ball`)
      this.setState({ isError: false, isLoading: false, data: response.data })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  logoutHandler(){
    this.props.navigation.navigate('Login')
  }
  AboutHandler(){
    this.props.navigation.navigate('About')
  }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (
      <View style={styles.container}>
          <View style={styles.profile}>
        <TouchableOpacity onPress={() => this.AboutHandler()}>
              <View style={{flexDirection:"row", alignItems:"center"}}>
                  <FontAwesome5 style={{marginStart:10}} name="user" size={24} color="black" />
                  <View style={{marginStart:10}}>
                      <Text style={{fontWeight:"bold"}}>Welcome</Text>
                      <Text>{this.props.route.params.userName}</Text>
                  </View>
              </View>
        </TouchableOpacity>
            <TouchableOpacity onPress={() => this.logoutHandler()} style={{marginEnd:8}}>
                  <MaterialCommunityIcons  name="logout" size={24} color="black" style={{alignSelf:"center"}}/>
                  <Text style={{textAlign:"center"}}>Logout</Text>
            </TouchableOpacity>
          </View>
          <FlatList style={{marginStart:10, marginEnd:10}}
            data={this.state.data.Search}
            renderItem={({item}) => (
              <TouchableOpacity key={item.imdbID} onPress={() => this.props.navigation.navigate('Detail', {imdbID : item.imdbID})} style={styles.itemlist}>
                <Image source={{ uri: `${item.Poster}` }} style={styles.Image} />
                <View style={styles.Text}>
                    <Text style={{fontWeight:"bold"}}>{item.Title}</Text>
                    <Text>{item.Year}</Text>
                    <Text>{item.Type}</Text>
                </View>
              </TouchableOpacity>
            )}
            />
      </View>
      );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    profile:{
      flexDirection:"row",
      backgroundColor:"#ebeded",
      marginStart:16,
      marginEnd:16,
      marginTop:8,
      alignItems:"center",
      borderRadius:8,
      justifyContent:"space-between",
      height:80
    },
  itemlist:{
    marginBottom:8,
    flexDirection:"row",
    borderRadius:8,
    borderWidth:1
  },
  Image:{
    width: 100,
    height: 120,
    margin:8,
    resizeMode:"center",
  },
  Text:{
      marginTop:20,
  }
})