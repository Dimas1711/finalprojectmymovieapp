import Axios from 'axios';
import React, { Component } from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, View, Image, ScrollView } from 'react-native';

export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isLoading: true,
            isError: false,
        };
    }
    
    
      // Mount User Method
  componentDidMount() {
    this.getMovieDataDetail()
  }

  //   Get Api Users
  getMovieDataDetail = async () => {
    try {
      const response = await Axios.get(`http://www.omdbapi.com/?apikey=133decb5&i=`+ this.props.route.params.imdbID)
      console.log(response)
      this.setState({ isError: false, isLoading: false, data: response.data})
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    const item = this.state.data
    return (
      <View style={styles.container}>
        <ScrollView>
            <Image source={{ uri: `${item.Poster}` }} style={styles.image} resizeMode={"contain"} />
            <Text style={styles.title}>{item.Title}</Text>
            <View style={{flexDirection:"row", margin:8}}>
              <Text style={styles.subtitle}>Year</Text>
              <Text style={{marginStart:20}}>:</Text>
              <Text style={{marginEnd:50, marginStart:8}}>{item.Year}</Text>
            </View>
            <View style={{flexDirection:"row", margin:8}}>
              <Text style={styles.subtitle}>Genre</Text>
              <Text style={{marginStart:10}}>:</Text>
              <Text style={{marginEnd:50, marginStart:8}}>{item.Genre}</Text>
            </View>
            <View style={{flexDirection:"row", margin:8}}>
              <Text style={styles.subtitle}>Plot</Text>
              <Text style={{marginStart:20}}>:</Text>
              <Text style={{marginEnd:50, marginStart:8}}>{item.Plot}</Text>
            </View>
            <View style={{flexDirection:"row", margin:8}}>
              <Text style={styles.subtitle}>Rated</Text>
              <Text style={{marginStart:10}}>:</Text>
              <Text style={{marginEnd:50, marginStart:8}}>{item.Rated}</Text>
            </View>
            <View style={{flexDirection:"row", margin:8}}>
              <Text style={styles.subtitle}>Rating</Text>
              <Text style={{marginStart:8}}>:</Text>
              <Text style={{marginEnd:50, marginStart:8}}>{item.imdbRating}</Text>
            </View>
        </ScrollView>
      </View>
      );
    }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
  },
  text:{
    flexDirection:"row"
  },
  title:{
    alignSelf:"center",
    fontSize:20,
    fontWeight:"bold"
  },
  subtitle:{
    marginStart:3,
    fontWeight:"bold"
  },
  image:{
    margin:8,
    width:350,
    height:500,
    alignSelf:"center"
  }
})
